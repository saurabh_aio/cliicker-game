import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl = 'http://localhost:5000/users'; // The URL to your API

  constructor(private http: HttpClient) {}

  saveUser(user: any): Observable<any> {
    return this.http.post<any>(this.apiUrl, user);
  }

  loginUser(user: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/login`, user);
  }
}
