import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { HomeComponent } from './components/home/home.component';
import { GamesModule } from './modules/games/games.module';
import { GameCategoriesComponent } from './categories-and-menu/game-categories/game-categories.component';
import { GameMenuComponent } from './categories-and-menu/game-menu/game-menu.component';
import { UserService } from './services/user-services/user.service';
import { RegistrationModule } from './modules/registration/registration.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginModule } from './modules/login/login.module';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    HomeComponent,
    GameCategoriesComponent,
    GameMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule,CommonModule,GamesModule,RegistrationModule, HttpClientModule, LoginModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
