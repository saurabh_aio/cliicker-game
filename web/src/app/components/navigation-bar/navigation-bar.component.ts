import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrl: './navigation-bar.component.scss'
})
export class NavigationBarComponent {
  constructor(private router:Router){}
  @Output() selectedPageEmitter:EventEmitter<string> = new EventEmitter<string>();

  // Define your navigation buttons here
  navigationButtons = [
    { label: 'Home', page: 'home' },
    { label: 'Games', page: 'games' }   
  ];

  emitSelectedPage(page){
    this.router.navigate(['/'+page]);
    this.selectedPageEmitter.emit(page);
  }

 

}
