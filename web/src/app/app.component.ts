import { Component, ViewChild } from '@angular/core';
import { CellsComponent } from './modules/games/games-common-components/cells/cells.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(){};
  selectedPage = 'home'
  selectPage(page: any) {
    this.selectedPage = page;
  }
}
