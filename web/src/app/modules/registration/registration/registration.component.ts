import { Component } from '@angular/core';
import { UserService } from '../../../services/user-services/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  user = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    name: ''
  };

  constructor(private userService: UserService) {}

  onSubmit() {
    if (this.user.password !== this.user.confirmPassword) {
      alert('Passwords do not match!');
      return;
    }

    this.user.name = this.user.firstName + ' '+ this.user.lastName;

    this.userService.saveUser(this.user).subscribe(
      response => {
        console.log('User registered', response);
        alert('Registration successful!');
        this.clearForm();
      },
      error => {
        console.error('Error registering user', error);
        alert('Registration failed. Please try again.');
      }
    );
  }

  clearForm() {
    this.user = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      name: ''
    };
  }
}
