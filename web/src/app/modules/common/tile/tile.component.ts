import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tile', 
  templateUrl: './tile.component.html',
  styleUrl: './tile.component.scss'
})
export class TileComponent {
  constructor(private router:Router){}
  @Input() tiles:any = [];
  goto(path){
    alert(path);
    this.router.navigate(['/'+path]);

  }
}
