import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindTheObjectGameComponent } from './find-the-object-game.component';

describe('FindTheObjectGameComponent', () => {
  let component: FindTheObjectGameComponent;
  let fixture: ComponentFixture<FindTheObjectGameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FindTheObjectGameComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FindTheObjectGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
