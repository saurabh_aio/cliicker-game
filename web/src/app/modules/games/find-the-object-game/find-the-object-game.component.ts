import { Component, ViewChild } from '@angular/core';
import { CellsComponent } from 'src/app/modules/games/games-common-components/cells/cells.component';

@Component({
  selector: 'app-find-the-object-game',
  templateUrl: './find-the-object-game.component.html',
  styleUrl: './find-the-object-game.component.scss'
})
export class FindTheObjectGameComponent {
  @ViewChild('cells') cellsComponent: CellsComponent;

  title = 'fun-games';
  currentNumber: any=-1;
  score: any;
  started: boolean;
  timeLeft: number;  
  countDown: any;
  interval: any;
  setScore(event) {
    if (!this.started) {
      return false;
    }
    console.log(event.target.id);
    console.log(this.cellsComponent.cells[event.target.id]);
    if (this.cellsComponent.cells[event.target.id].value == this.currentNumber && !this.cellsComponent.cells[event.target.id].clicked) {
      this.score++;
      this.cellsComponent.cells[event.target.id].clicked = true;
      document.getElementById(event.target.id).style.background = '#009900';
      document.getElementById(event.target.id).style.border = 'solid 1px green';
    };
  }

  start() {
    this.cellsComponent.generateCells(false);
    this.countDown = 3;
    clearInterval(this.interval);
    this.timeLeft = 10;
    this.started = false;
    this.cellsComponent.started = false;
    this.score = 0;

    this.started = true;
    this.cellsComponent.generateCells(true);
    var cell = this.cellsComponent.cells[Math.floor(Math.random() * 100)];
    this.currentNumber = cell.value;
    this.interval = setInterval(() => {
      this.countDown--;
      if (this.countDown <= 0) {
        clearInterval(this.interval);
        this.cellsComponent.started=true;
        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          } else {
            this.started = false;
            for (var i = 0; i < this.cellsComponent.cells.length; i++) {
              if (this.cellsComponent.cells[i].value == this.currentNumber) {
                // this.cellsComponent.cells[i].clicked=true;
                if (this.cellsComponent.cells[i].clicked == false) {
                  document.getElementById(i + '').style.border = 'solid 1px red';
                  document.getElementById(i + '').style.background = '#000';

                }else{
                  document.getElementById(i + '').style.border = 'solid 1px green';
                  document.getElementById(i + '').style.background = '#009900';
                }
              }else{
                this.cellsComponent.cells[i].value=-1;
              }

            }

          }

        }, 1000);

      }
    }, 1000)




  }

}
