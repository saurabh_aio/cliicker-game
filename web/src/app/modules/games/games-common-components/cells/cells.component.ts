import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
const level =1;

@Component({
  selector: 'app-cells',
  templateUrl: './cells.component.html',
  styleUrls: ['./cells.component.scss']
})
export class CellsComponent implements OnInit {
  @Output() eventEmitter = new EventEmitter();
  constructor() { }
  cells:any=[];
  started=false;

  ngOnInit(): void {
    this.generateCells(false);
  }

  generateCells(status){
    for(var i=0;i<level*100;i++){
      if(status)
      this.cells[i]={value:Math.floor(Math.random()*10),clicked:false};
      else
      this.cells[i]={value:'',clicked:false};      
    }
  }

  emitValue(event,id){
   event.target.id =id;

  }


}
