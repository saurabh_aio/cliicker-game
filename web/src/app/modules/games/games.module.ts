import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GamesRoutingModule } from './games-routing.module';
import { FindTheObjectGameComponent } from './find-the-object-game/find-the-object-game.component';
import { CellsComponent } from './games-common-components/cells/cells.component';
import { TileModule } from '../common/tile/tile.module';
import { GamesHomeComponent } from './games-home/games-home.component';
import { ChataiComponent } from './chatai/chatai.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [
    FindTheObjectGameComponent, CellsComponent, GamesHomeComponent, ChataiComponent
  ],
  imports: [
    CommonModule,FontAwesomeModule,
    GamesRoutingModule, TileModule
  ]
})
export class GamesModule { }
