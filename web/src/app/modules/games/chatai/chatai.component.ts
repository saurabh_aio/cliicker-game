import { Component, OnInit } from '@angular/core';
import { faCoffee, faHeart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-chatai',
  templateUrl: './chatai.component.html',
  styleUrl: './chatai.component.scss'
})
export class ChataiComponent implements OnInit {
  
  faHeart = faHeart;
  chances = 2;
  playerBlue  = {
    name:'Blue',
    score:0
  };
  playerRed = {
    name:'Red',
    score:0
  };
  turnIsOf = 'red'
  level = 2;
  numberOfCards = 4;
  gameStarted = true;
  deck:any = [
    { id: 1, opacity:1, status: 0, value: 'A', name: 'Ace of Hearts', color: 'Red', type: 'Heart' },
    { id: 2, opacity:1, status: 0, value: 2, name: '2 of Hearts', color: 'Red', type: 'Heart' },
    { id: 3, opacity:1, status: 0, value: 3, name: '3 of Hearts', color: 'Red', type: 'Heart' },
    { id: 4, opacity:1, status: 0, value: 4, name: '4 of Hearts', color: 'Red', type: 'Heart' },
    { id: 5, opacity:1, status: 0, value: 5, name: '5 of Hearts', color: 'Red', type: 'Heart' },
    { id: 6, opacity:1, status: 0, value: 6, name: '6 of Hearts', color: 'Red', type: 'Heart' },
    { id: 7, opacity:1, status: 0, value: 7, name: '7 of Hearts', color: 'Red', type: 'Heart' },
    { id: 8, opacity:1, status: 0, value: 8, name: '8 of Hearts', color: 'Red', type: 'Heart' },
    { id: 9, opacity:1, status: 0, value: 9, name: '9 of Hearts', color: 'Red', type: 'Heart' },
    { id: 10, opacity:1, status: 0, value: 10, name: '10 of Hearts', color: 'Red', type: 'Heart' },
    { id: 11, opacity:1, status: 0, value: 'J', name: 'Jack of Hearts', color: 'Red', type: 'Heart' },
    { id: 12, opacity:1, status: 0, value: 'Q', name: 'Queen of Hearts', color: 'Red', type: 'Heart' },
    { id: 13, opacity:1, status: 0, value: 'K', name: 'King of Hearts', color: 'Red', type: 'Heart' },
    { id: 14, opacity:1, status: 0, value: 'A', name: 'Ace of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 15, opacity:1, status: 0, value: 2, name: '2 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 16, opacity:1, status: 0, value: 3, name: '3 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 17, opacity:1, status: 0, value: 4, name: '4 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 18, opacity:1, status: 0, value: 5, name: '5 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 19, opacity:1, status: 0, value: 6, name: '6 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 20, opacity:1, status: 0, value: 7, name: '7 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 21, opacity:1, status: 0, value: 8, name: '8 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 22, opacity:1, status: 0, value: 9, name: '9 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 23, opacity:1, status: 0, value: 10, name: '10 of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 24, opacity:1, status: 0, value: 'J', name: 'Jack of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 25, opacity:1, status: 0, value: 'Q', name: 'Queen of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 26, opacity:1, status: 0, value: 'K', name: 'King of Diamonds', color: 'Red', type: 'Diamond' },
    { id: 27, opacity:1, status: 0, value: 'A', name: 'Ace of Clubs', color: 'Black', type: 'Club' },
    { id: 28, opacity:1, status: 0, value: 2, name: '2 of Clubs', color: 'Black', type: 'Club' },
    { id: 29, opacity:1, status: 0, value: 3, name: '3 of Clubs', color: 'Black', type: 'Club' },
    { id: 30, opacity:1, status: 0, value: 4, name: '4 of Clubs', color: 'Black', type: 'Club' },
    { id: 31, opacity:1, status: 0, value: 5, name: '5 of Clubs', color: 'Black', type: 'Club' },
    { id: 32, opacity:1, status: 0, value: 6, name: '6 of Clubs', color: 'Black', type: 'Club' },
    { id: 33, opacity:1, status: 0, value: 7, name: '7 of Clubs', color: 'Black', type: 'Club' },
    { id: 34, opacity:1, status: 0, value: 8, name: '8 of Clubs', color: 'Black', type: 'Club' },
    { id: 35, opacity:1, status: 0, value: 9, name: '9 of Clubs', color: 'Black', type: 'Club' },
    { id: 36, opacity:1, status: 0, value: 10, name: '10 of Clubs', color: 'Black', type: 'Club' },
    { id: 37, opacity:1, status: 0, value: 'J', name: 'Jack of Clubs', color: 'Black', type: 'Club' },
    { id: 38, opacity:1, status: 0, value: 'Q', name: 'Queen of Clubs', color: 'Black', type: 'Club' },
    { id: 39, opacity:1, status: 0, value: 'K', name: 'King of Clubs', color: 'Black', type: 'Club' },
    { id: 40, opacity:1, status: 0, value: 'A', name: 'Ace of Spades', color: 'Black', type: 'Spade' },
    { id: 41, opacity:1, status: 0, value: 2, name: '2 of Spades', color: 'Black', type: 'Spade' },
    { id: 42, opacity:1, status: 0, value: 3, name: '3 of Spades', color: 'Black', type: 'Spade' },
    { id: 43, opacity:1, status: 0, value: 4, name: '4 of Spades', color: 'Black', type: 'Spade' },
    { id: 44, opacity:1, status: 0, value: 5, name: '5 of Spades', color: 'Black', type: 'Spade' },
    { id: 45, opacity:1, status: 0, value: 6, name: '6 of Spades', color: 'Black', type: 'Spade' },
    { id: 46, opacity:1, status: 0, value: 7, name: '7 of Spades', color: 'Black', type: 'Spade' },
    { id: 47, opacity:1, status: 0, value: 8, name: '8 of Spades', color: 'Black', type: 'Spade' },
    { id: 48, opacity:1, status: 0, value: 9, name: '9 of Spades', color: 'Black', type: 'Spade' },
    { id: 49, opacity:1, status: 0, value: 10, name: '10 of Spades', color: 'Black', type: 'Spade' },
    { id: 50, opacity:1, status: 0, value: 'J', name: 'Jack of Spades', color: 'Black', type: 'Spade' },
    { id: 51, opacity:1, status: 0, value: 'Q', name: 'Queen of Spades', color: 'Black', type: 'Spade' },
    { id: 52, opacity:1, status: 0, value: 'K', name: 'King of Spades', color: 'Black', type: 'Spade' }
  ]
  chataiCards: any= [];
  deFadeInProcess: boolean = false;
  blockClick: boolean = false;

  ngOnInit(): void {
  }
  startGame(){
    this.chataiCards = [];
    this.deck = this.shuffleDeck(this.deck);
    this.setNumberOfCards();
    this.getSubSetCardsInPairs();

  }
  getSubSetCardsInPairs() {
    let tempCards = [];
    for(let i=0;i<this.numberOfCards;i++){
      let temp = this.deck[i];
      temp.opacity = 0;
      tempCards.push({...temp});
      tempCards.push({...temp});
    }
    tempCards = this.shuffleDeck(tempCards);
    this.chataiCards = [...tempCards]
  }
  setNumberOfCards() {
    this.numberOfCards = 2*this.level + 4;
  }
  setLevel(){
    if(this.level>5){
      this.level = 5;
    }
    if(this.level<=0){
      this.level = 1;
    }
  }
  shuffleDeck(deck) {
    for(let i=0;i<9999;i++){
    let val1 = Math.floor(Math.random()*deck.length);
    let val2 = Math.floor(Math.random()*deck.length);
    let temp = deck[val1];
    deck[val1] = deck[val2];
    deck[val2] = temp;
  }
  return deck;


  }

  setStatus(status, index){
    this.deck[index].status = status;
  }
  toggleStatus(index){
    if(this.chataiCards[index].satatus == 1)
      return;
    if(this.chataiCards[index].status == 0){
      this.chataiCards[index].status = 1;
      this.chataiCards[index].opacity = 1;
    }
    this.doChataiProcessing();
    this.chances--;
    if(this.chances == 0){
          this.turnIsOf = this.turnIsOf == 'blue'?'red':'blue';
          this.chances = 2;

    }
  }
  doChataiProcessing() {
    let opened1:any;
    let index1:any;
    let opened2:any;
    let index2:any;
    for(let i=0;i<this.chataiCards.length;i++){
      if(this.chataiCards[i].status == 1){
        if(!opened1){
          opened1= this.chataiCards[i];
          index1=i;
        }else{
          opened2 = this.chataiCards[i];
          index2=i;
        }
      }
    }
    if(opened1 && opened2){
      if(opened1.id == opened2.id){
        opened1.status ='x';
        opened2.status = 'x';
        if(this.turnIsOf == 'blue'){
          this.playerBlue.score++;
        }else{
          this.playerRed.score++;
        }
      } else {
        this.blockClick = true;
        let timeout = setTimeout(() => {
           opened1.status = 0;
        opened2.status = 0;
        this.chataiCards[index1].opacity = 0;
        this.chataiCards[index2].opacity = 0;
        clearTimeout(timeout);
        this.blockClick = false;
        }, 2000);
       
      }
    }
  }

  // fadeCard(cards,index){
  //   setTimeout(() => {
  //     cards[index].opacity = (cards[index].opacity*100-10)/100
  //     if(cards[index].opacity>0){
  //       this.fadeCard(cards,index);
  //     }      
  //   }, 50);
  // }
  // deFadeCard(cards,index){
  //   this.deFadeInProcess = true;
  //   setTimeout(() => {
  //     cards[index].opacity = (cards[index].opacity*100+10)/100
  //     if(cards[index].opacity<1){
  //       this.deFadeCard(cards,index);
  //     }else{
  //       this.deFadeInProcess = false;
  //     }      
  //   }, 50);
  // }

}
