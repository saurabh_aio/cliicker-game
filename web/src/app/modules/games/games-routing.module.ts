import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FindTheObjectGameComponent } from './find-the-object-game/find-the-object-game.component';
import { GamesHomeComponent } from './games-home/games-home.component';
import { ChataiComponent } from './chatai/chatai.component';

const routes: Routes = [
  {path:'', component:GamesHomeComponent},
  {path:'find-the-object', component:FindTheObjectGameComponent},
  {path:'chatai', component:ChataiComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamesRoutingModule { }
