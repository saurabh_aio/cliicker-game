import { Component } from '@angular/core';
import { UserService } from '../../services/user-services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginData = { email: '', password: '' };

  constructor(private userService: UserService) {}

  onSubmit() {
    this.userService.loginUser(this.loginData).subscribe(
      response => {
        console.log('Login successful', response);
        // You might want to store the token and redirect the user
        localStorage.setItem('token', response.token);
      },
      error => {
        console.error('Login failed', error);
      }
    );
  }
}
