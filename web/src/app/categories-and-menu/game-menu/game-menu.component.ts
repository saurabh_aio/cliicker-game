import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GameMenuItem } from '../categories-and-menu-modals/game-menu.modal'; 

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrl: './game-menu.component.scss'
})
export class GameMenuComponent {
  @Input() games:GameMenuItem[]  // List of games
  @Output() gameSelected = new EventEmitter<string>(); // Event emitter for game selection

  constructor() { }

  onSelectGame(game: string) {
    this.gameSelected.emit(game);
  }

}
