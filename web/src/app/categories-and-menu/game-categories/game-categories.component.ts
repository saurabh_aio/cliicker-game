import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GameMenuItem } from '../categories-and-menu-modals/game-menu.modal';

@Component({
  selector: 'app-game-categories',
  templateUrl: './game-categories.component.html',
  styleUrl: './game-categories.component.scss'
})
export class GameCategoriesComponent {
  categories: string[] = ["Puzzle","Click","Learning","Fun"]; // Array of game categories
  parentWidth: number;
  gamesMenuItems: GameMenuItem[] = [];

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.getParentWidth();
    this.setGamesMenuItems();
  }
  setGamesMenuItems() {
    let gameItem = new GameMenuItem();
    gameItem.name = "myGame";
    gameItem.route = "myGameRoute";
    this.gamesMenuItems.push(gameItem);
  }

  getParentWidth() {
    // Get the width of the parent container
    this.parentWidth = document.getElementById('grid-container').offsetWidth;
  }

  gamesList: string[] = ['Game 1', 'Game 2', 'Game 3']; // List of games


  navigateToGame(game: string) {
    // Navigate to the route of the selected game
    this.router.navigate(['/games', game]);
  }

}
